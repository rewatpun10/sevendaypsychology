import { Component, OnInit } from '@angular/core';
import {UserServiceService} from '../services/user-service.service';
import {ModalService} from '../_modal';

@Component({
  selector: 'app-counsellors',
  templateUrl: './counsellors.component.html',
  styleUrls: ['./counsellors.component.css']
})
export class CounsellorsComponent implements OnInit {
  counsellors;
  counsellorId;
  constructor(
    private userService: UserServiceService,
    private modalService: ModalService
  ) { }

  ngOnInit() {
    this.userService.getAllCounsellors()
      .subscribe(data => {
        this.counsellors = data;
      });
  }

  openModal(id: string, bId: any) {
    this.modalService.open(id, bId);
  }

  closeModal(id: string) {
    this.modalService.close(id);
  }

  deleteCounsellor(id: string) {
    this.counsellorId = this.modalService.getDeleteId();
    this.userService.deleteUser(this.counsellorId);
    this.modalService.close(id);
  }
  downloadExcelFileOfCounsellors() {
    this.userService.downloadExcelFileOfCounsellor();
  }

}
