import { Component, OnInit } from '@angular/core';
import {BookingServiceService} from '../services/booking-service.service';
import {ActivatedRoute, Router} from '@angular/router';
import {UserServiceService} from '../services/user-service.service';
import {ModalService} from '../_modal';

@Component({
  selector: 'app-booked-appointments',
  templateUrl: './booked-appointments.component.html',
  styleUrls: ['./booked-appointments.component.css']
})
export class BookedAppointmentsComponent implements OnInit {
  bookedAppointments;
  user;
  bookedAppointmentDetails: any = [];
  constructor(private bookingService: BookingServiceService,
              private router: Router,
              private route: ActivatedRoute,
              private userService: UserServiceService,
              private modalService: ModalService) { }

  ngOnInit() {
    this.user = this.userService.getUser();
    this.bookingService.getBookedAppointment(this.user.id)
      .subscribe(data => {
        this.bookedAppointments = data;
    });
  }
  openModal(id: string, bId: any) {
    this.userService.getBookedAppointmentUserDetails(bId)
      .subscribe(bookedData => {
        this.bookedAppointmentDetails = bookedData;
      });
    this.modalService.open(id, bId);
  }

  closeModal(id: string) {
    this.modalService.close(id);
  }

}
