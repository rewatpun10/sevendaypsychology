import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import {RouterModule} from '@angular/router';
import { HomeComponent } from './home/home.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { FooterComponent } from './footer/footer.component';
import {FormsModule} from '@angular/forms';
import { HttpClientModule} from '@angular/common/http';
import { RegisterComponent } from './register/register.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SideNavComponent } from './side-nav/side-nav.component';
import { PersonalInformationComponent } from './personal-information/personal-information.component';
import { MyAvailabilityComponent } from './my-availability/my-availability.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';
import {CommonModule} from '@angular/common';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { ModalModule } from './_modal';
import { BookNewAppointmentComponent } from './book-new-appointment/book-new-appointment.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { BookedAppointmentsComponent } from './booked-appointments/booked-appointments.component';
import { CounsellorsComponent } from './counsellors/counsellors.component';
import { ClientsComponent } from './clients/clients.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    NavBarComponent,
    FooterComponent,
    RegisterComponent,
    DashboardComponent,
    SideNavComponent,
    PersonalInformationComponent,
    MyAvailabilityComponent,
    BookNewAppointmentComponent,
    BookedAppointmentsComponent,
    CounsellorsComponent,
    ClientsComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    CommonModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    ModalModule,
    NgSelectModule,
    FormsModule,
    RouterModule.forRoot([
      {path: '', component: HomeComponent},
      {path: 'home', component: HomeComponent},
      {path: 'signIn', component: LoginComponent},
      {path: 'register', component: RegisterComponent},
      {path: 'dashboard', component: DashboardComponent},
      {path: 'personal-information/:id', component: PersonalInformationComponent},
      {path: 'my-availability', component: MyAvailabilityComponent},
      {path: 'book-newAppointment', component: BookNewAppointmentComponent},
      {path: 'booked-appointments', component: BookedAppointmentsComponent},
      {path: 'counsellors', component: CounsellorsComponent},
      {path: 'clients', component: ClientsComponent},
    ]),
    FormsModule
  ],
  providers: [],
  entryComponents: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
