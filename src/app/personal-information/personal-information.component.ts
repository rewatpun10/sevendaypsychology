import { Component, OnInit } from '@angular/core';
import {UserServiceService} from '../services/user-service.service';
import {ActivatedRoute, Router} from '@angular/router';
import 'rxjs';

@Component({
  selector: 'app-personal-information',
  templateUrl: './personal-information.component.html',
  styleUrls: ['./personal-information.component.css']
})
export class PersonalInformationComponent implements OnInit {
  user;
  mainUser;
  id;
  constructor(private userService: UserServiceService,
              private router: Router,
              private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.mainUser = this.userService.getUser();
    this.id = this.route.snapshot.paramMap.get('id');
    if (this.id) {
        this.userService.getUserDetails(this.id).subscribe(data => {
          this.user = data;
        });
    }
  }

  update(values: any) {
    values.id  = this.user.id;
    this.userService.updateUser(values);
  }
}
