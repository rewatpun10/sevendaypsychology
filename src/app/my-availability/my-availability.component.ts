import {Component, OnChanges, OnInit} from '@angular/core';
import {UserServiceService} from '../services/user-service.service';
import {ActivatedRoute, Router} from '@angular/router';
import {DatePipe} from '@angular/common';
import {BookingServiceService} from '../services/booking-service.service';
import {BookingDetailsDTO} from '../models/BookingDetailsDTO';
import { ModalService } from '../_modal';

@Component({
  selector: 'app-my-availability',
  templateUrl: './my-availability.component.html',
  styleUrls: ['./my-availability.component.css'],
  providers: [DatePipe]
})
export class MyAvailabilityComponent implements OnInit, OnChanges {
  id;
  bookedDates;
  dateSelected = [];
  selectedClass = [];
  bookingDetails = [];
  bookedDateSelected = [];
  bookingDto = BookingDetailsDTO;
  public selectedMoment: any;
  myDate;
  private user: any;
  bodyText: string;
  bookedId;
  constructor(private userService: UserServiceService,
              private bookingService: BookingServiceService,
              private router: Router,
              private route: ActivatedRoute,
              private datePipe: DatePipe,
              private modalService: ModalService) {
    this.myDate = this.datePipe.transform(this.myDate, 'yyyy-MM-dd');
  }

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id');
    this.user = this.userService.getUser();
    this.bookedDates = this.bookingService.getBookedDateByCounsellor(this.user.id)
      .subscribe(data => {
        this.bookedDates = data;
      });
    this.bookedDateSelected.push(this.bookedDates);
  }
  getDateItem(date: Date): string {
    return `${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()}`;
  }

  onValueChange(event) {
    if (event.length === undefined) {
      const date = this.getDateItem(event);

      const index = this.dateSelected.findIndex(item => {
        const testDate = this.getDateItem(item);
        return testDate === date;
      });
      if (index < 0) {
        this.dateSelected.push(event);
      } else {
        this.dateSelected.splice(index, 1);
      }
    }


    if (this.dateSelected.length > 0) {
      this.selectedClass = this.dateSelected.map(date => {
        return {
          date,
          classes: ['custom-selected-date']
        };
      });
    }
  }
  ngOnChanges(event) {
  }

  addCurrentDateToList() {
    this.dateSelected.push(JSON.stringify(this.selectedMoment));
    this.selectedMoment = null;
  }

  removeBookedDate(data) {
  this.dateSelected.forEach( (myObject, index) => {
    if (myObject === data) {
      this.dateSelected.splice(index, 1);
    }
  });


  }

  saveBookedDate() {
    this.bookingDetails.push(this.dateSelected);
    // this.bookingDetails.push(this.user.id);
    // @ts-ignore
    this.bookingDto = new BookingDetailsDTO(this.user.id, this.dateSelected);
    this.bookingService.bookAvailability(this.bookingDto);
  }

  openModal(id: string, bId: any) {
    this.modalService.open(id, bId);
  }

  closeModal(id: string) {
    this.modalService.close(id);
  }
  deleteBookedDate(id: string) {
   this.bookedId = this.modalService.getDeleteId();
   this.bookingService.deleteAvailability(this.bookedId);
   this.modalService.close(id);
  }
}
