import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyAvailabilityComponent } from './my-availability.component';

describe('MyAvailabilityComponent', () => {
  let component: MyAvailabilityComponent;
  let fixture: ComponentFixture<MyAvailabilityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyAvailabilityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyAvailabilityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
