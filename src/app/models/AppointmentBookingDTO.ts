export class AppointmentBookingDTO {
  id: string
  availabilityTime: string;
  counsellorId: string;
  status: string;
  clientId: string;
  constructor(id, availabilityTime, counsellorId, status, clientId ) {
    this.id = id;
    this.availabilityTime = availabilityTime;
    this.counsellorId = counsellorId;
    this.status = status;
    this.clientId = clientId;
}
}
