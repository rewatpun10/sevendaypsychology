export class UserDTO {
  username: string;
  firstName: string;
  middleName: string;
  lastName: string;
  role: string;
  status: string;
  mobileNumber: string;
  description: string;
  constructor(username, firstName, lastName, role, status, mobileNumber, description ) {
    this.username = username;
    this.lastName = lastName;
    this.role = role;
    this.status = status;
    this.mobileNumber = mobileNumber;
    this.description = description;
  }
}
