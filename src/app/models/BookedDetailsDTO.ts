export class BookedDetailsDTO {
  bookedId: string;
  clientId: string;
  bookingAvailabilityId: string;
}
