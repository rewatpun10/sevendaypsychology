export class BookingDetailsDTO {
  counsellorId: number;
  availabilityDate: any;
  constructor(counsellorId, availabilityDate) {
  this.counsellorId = counsellorId;
  this.availabilityDate = availabilityDate;
}
}
