import { Component, OnInit } from '@angular/core';
import {UserServiceService} from '../services/user-service.service';
import {ModalService} from '../_modal';

@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.css']
})
export class ClientsComponent implements OnInit {
  clients;
  clientId;
  constructor( private userService: UserServiceService,
               private modalService: ModalService) { }

  ngOnInit() {
    this.userService.getAllClients()
      .subscribe(data => {
        this.clients = data;
      });
  }

  openModal(id: string, bId: any) {
    this.modalService.open(id, bId);
  }

  closeModal(id: string) {
    this.modalService.close(id);
  }

  deleteClient(id: string) {
    this.clientId = this.modalService.getDeleteId();
    this.userService.deleteUser(this.clientId);
    this.modalService.close(id);
  }

  downloadExcelFileOfClients() {
    this.userService.downloadExcelFileOfClients();
  }
}
