import { Component, OnInit } from '@angular/core';
import {UserServiceService} from '../services/user-service.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  user: any = [];
  constructor(private userService: UserServiceService) { }

  ngOnInit() {
    this.user = this.userService.getUser();
  }

  login(value: any) {
    this.userService.login(value.username, value.password);
  }
}
