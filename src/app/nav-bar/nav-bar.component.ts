import { Component, OnInit } from '@angular/core';
import {UserServiceService} from '../services/user-service.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {
  user: any = [];
  constructor(private userService: UserServiceService, private route: Router) { }

  ngOnInit() {
    this.user = this.userService.getUser();
  }

  logout() {
    this.user = null;
    localStorage.removeItem('userDetails');
    this.route.navigateByUrl('/home');
  }
}
