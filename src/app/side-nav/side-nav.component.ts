import { Component, OnInit } from '@angular/core';
import {UserServiceService} from '../services/user-service.service';

@Component({
  selector: 'app-side-nav',
  templateUrl: './side-nav.component.html',
  styleUrls: ['./side-nav.component.css']
})
export class SideNavComponent implements OnInit {
  user: any =[];
  constructor(private userService: UserServiceService) { }
  ngOnInit() {
    this.user = this.userService.getUser();
  }

}
