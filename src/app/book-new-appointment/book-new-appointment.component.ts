import { Component, OnInit } from '@angular/core';
import {BookingServiceService} from '../services/booking-service.service';
import {ActivatedRoute, Router} from '@angular/router';
import {UserServiceService} from '../services/user-service.service';
import {AppointmentBookingDTO} from '../models/AppointmentBookingDTO';

@Component({
  selector: 'app-book-new-appointment',
  templateUrl: './book-new-appointment.component.html',
  styleUrls: ['./book-new-appointment.component.css']
})
export class BookNewAppointmentComponent implements OnInit {
  bookingAvailable;
  bookingSelected: any;
  user;
  appointmentDto;
  constructor( private bookingService: BookingServiceService,
               private router: Router,
               private route: ActivatedRoute,
               private userService: UserServiceService) { }

  ngOnInit() {
    this.user = this.userService.getUser();
    this.bookingService.getAllAvailableAppointment()
      .subscribe(data => {
        this.bookingAvailable = data;
      });
  }

  confirmBookDate() {
    // tslint:disable-next-line:max-line-length
    this.appointmentDto = new AppointmentBookingDTO(this.bookingSelected.id, this.bookingSelected.availabilityTime, this.bookingSelected.counsellorId, this.bookingSelected.status, this.user.id);
    this.bookingService.bookAppointmentsForClient(this.appointmentDto);
  }
}
