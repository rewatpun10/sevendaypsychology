import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookNewAppointmentComponent } from './book-new-appointment.component';

describe('BookNewAppointmentComponent', () => {
  let component: BookNewAppointmentComponent;
  let fixture: ComponentFixture<BookNewAppointmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookNewAppointmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookNewAppointmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
