import {Component, HostListener, OnInit, ViewChild} from '@angular/core';
import {UserDTO} from '../models/UserDTO';
import {Observable} from 'rxjs';
import {UserServiceService} from '../services/user-service.service';
import {User} from '../user';
import {ActivatedRoute, Router} from '@angular/router';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  private user: any;
  userDetails: any = [];
  constructor(private  userService: UserServiceService, private route: ActivatedRoute, private router: Router) {
  }
  ngOnInit() {
    this.user = this.userService.getUser();
    this.userService.getUserDetails(this.user.id)
      .subscribe(userData => {
        this.userDetails = userData;
      });
  }
}
