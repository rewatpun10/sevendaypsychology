import { Component, OnInit } from '@angular/core';
import {UserServiceService} from '../services/user-service.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  user: any = [];
  constructor(private userService: UserServiceService) { }

  ngOnInit() {
    this.user = this.userService.getUser();
  }

  register(userDetails: any) {
    this.userService.registerUser(userDetails);
  }
}
