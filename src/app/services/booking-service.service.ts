import { Injectable } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {UserDTO} from '../models/UserDTO';
import {BookingDetailsDTO} from '../models/BookingDetailsDTO';
import {BookedDetailsDTO} from '../models/BookedDetailsDTO';

@Injectable({
  providedIn: 'root'
})
export class BookingServiceService {

  url = 'http://localhost:8080';
  constructor(private route: ActivatedRoute, private router: Router, private http: HttpClient) { }
  bookAvailability(bookingDetails) {
    this.http.post<Observable<boolean>>(this.url  + '/createBookingAvailability', bookingDetails)
      .subscribe(savedBookingDetails => {
        if (savedBookingDetails) {
          window.location.reload();
          this.router.navigateByUrl('/my-availability');
        } else {
          alert(' Something went wrong. Please try again');
        }
      });
  }

  getBookedDateByCounsellor(id) {
    return this.http.get<Observable<boolean>>(this.url + '/getBookedDateByCounsellorId/' + id);
  }

  deleteAvailability(id) {
    this.http.delete(this.url + '/deleteBookedDateById/' + id).
    subscribe((data) => {
      window.location.reload();
    });
  }

  getAllAvailableAppointment() {
    return this.http.get(this.url + '/availableBookedDates');
  }

  bookAppointmentsForClient(appointmentDto: any) {
    this.http.post<Observable<BookedDetailsDTO>>(this.url  + '/bookAppointmentForClient', appointmentDto)
      .subscribe(bookedDetails => {
        if (bookedDetails) {
          alert(' Appointment Booked Successfully');
          this.router.navigateByUrl('/booked-appointments');
        } else {
          alert(' Something went wrong. Please try again');
        }
      });
  }

  getBookedAppointment(id) {
    return this.http.get<Observable<boolean>>(this.url + '/clientBookedDatesById/' + id);
  }
}
