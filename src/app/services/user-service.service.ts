import {Injectable, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {ActivatedRoute, Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {UserDTO} from '../models/UserDTO';
import {catchError, tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserServiceService {

  user = this.getUser();
  constructor(private route: ActivatedRoute, private router: Router, private http: HttpClient) { }
  url = 'http://localhost:8080';
  login(username: string, password: string) {
    this.http.post<Observable<UserDTO>>(this.url + '/login', {username, password})
      .subscribe(userDetails => {
        if (userDetails !== null) {
        localStorage.setItem('userDetails', JSON.stringify(userDetails));
        this.router.navigate(['/dashboard']);
      } else {
        alert('Authentication failed.');
      }
    });
  }

  registerUser(userDetails: any) {
    this.http.post(this.url  + '/createUser', userDetails,  {responseType: 'text'})
      .subscribe(message => {
        if (message !== 'duplicate') {
          if (this.getUser() !== null && this.getUser().role === 'admin' && userDetails.role === 'counsellor') {
            this.router.navigateByUrl('/counsellors');
            alert(' User successfully registered');
          } else if (this.getUser() !== null && this.getUser().role === 'admin' && userDetails.role === 'client') {
            this.router.navigateByUrl('/clients');
            alert(' User successfully registered.');
          } else {
            this.router.navigateByUrl('/signIn');
            alert(' User successfully registered. Please login with your credentials');
          }
        } else {
          alert('Username already exists');
        }
      });
  }

  getUserDetails(id) {
    return this.http.get<UserDTO>(this.url + '/getUserById/' + id);
  }
  getUser(): any {
    const user = JSON.parse(localStorage.getItem('userDetails'));
    return user;
  }

  getAllCounsellors() {
    return this.http.get<UserDTO>(this.url + '/getAllCounsellors');
  }

  updateUser(values: any) {
    this.http.put<Observable<boolean>>(this.url  + '/updateUser', values)
      .subscribe(isUpdated => {
        if (isUpdated) {
          alert(' User successfully updated');
          if (this.user.role === 'admin' && values.role === 'counsellor') {
            this.router.navigateByUrl('/counsellors');
          } else if (this.user.role === 'admin' && values.role === 'client') {
            this.router.navigateByUrl('/clients');
          } else {
            this.router.navigateByUrl('/dashboard');
          }
        } else {
          alert(' Something went wrong. Please try again');
        }
      });
  }

  getAllClients() {
    return this.http.get<UserDTO>(this.url + '/getAllClients');

  }

  deleteUser(id: any) {
    this.http.delete(this.url + '/deleteUser/' + id)
      .subscribe((data) => {
      window.location.reload();
    });
  }

  getBookedAppointmentUserDetails(bId: any) {
    return this.http.get(this.url + '/getBookedAppointmentUserDetails/' + bId);
  }

  downloadExcelFileOfCounsellor() {
    return this.http.get(this.url + '/downloadExcelFileOfCounsellor', { responseType: 'blob'}).subscribe(data => {
      const blob = new Blob([data], { type: 'application/vnd.ms-excel' });
      const url = window.URL.createObjectURL(blob);
      window.open(url);
    });
  }

  downloadExcelFileOfClients() {
    return this.http.get(this.url + '/downloadExcelFileOfClient', { responseType: 'blob'}).subscribe(data => {
      const blob = new Blob([data], { type: 'application/vnd.ms-excel' });
      const url = window.URL.createObjectURL(blob);
      window.open(url);
    });
  }
}
